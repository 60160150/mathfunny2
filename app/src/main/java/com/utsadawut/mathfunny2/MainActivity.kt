package com.utsadawut.mathfunny2

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_main.view.txtShowAns
import java.util.*
import kotlin.random.Random.Default.nextInt
public var correct: Int = 0
public var incorrect: Int = 0
public var isSelectAnswer = false

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        play()
    }

    fun play() {
        val textNumber1 = findViewById<TextView>(R.id.number1)
        val textNumber2 = findViewById<TextView>(R.id.number2)
        val txtStatic = findViewById<TextView>(R.id.txttrue)

        val btnAns1 = findViewById<Button>(R.id.btnAns1)
        val btnAns2 = findViewById<Button>(R.id.btnAns2)
        val btnAns3 = findViewById<Button>(R.id.btnAns3)
        val btnPlay = findViewById<Button>(R.id.btnAgain)
        btnPlay.visibility = View.GONE
        isSelectAnswer = false

        val txtShow = findViewById<TextView>(R.id.txtShowAns)

        textNumber1.text = (0..10).random().toString()
        textNumber2.text = (0..10).random().toString()

        val num1 = (textNumber1).text.toString().toInt()
        val num2 = (textNumber2).text.toString().toInt()
        val ans = num1 + num2
        val ran = (1..3).random().toString()
        val ran1 = (1..3).random().toString()
        val ran2 = (1..3).random().toString()

        if (ran == "1") {
            btnAns1.text = ans.toString()
            btnAns2.text = (0..(ans - 1)).random().toString()
            btnAns3.text = ((ans + 1)..20).random().toString()
        } else if (ran == "2") {
            btnAns2.text = ans.toString()
            btnAns1.text = ((ans + 1)..20).random().toString()
            btnAns3.text = (0..(ans - 1)).random().toString()
        } else {
            btnAns3.text = ans.toString()
            btnAns2.text = (0..(ans - 1)).random().toString()
            btnAns1.text = ((ans + 1)..20).random().toString()
        }

        btnAns1.setBackgroundColor(Color.GRAY)
        btnAns2.setBackgroundColor(Color.GRAY)
        btnAns3.setBackgroundColor(Color.GRAY)

        btnAns1.setOnClickListener {
            if (ran == "1") {

                if(!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }

            } else {

                if(!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            }
        }
        btnAns2.setOnClickListener {
            if (ran == "2") {

                if(!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            } else {


                if(!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true
                }
            }
        }
        btnAns3.setOnClickListener {
            if (ran == "3") {

                if(!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            } else {

                if(!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            }
        }
        btnPlay.setOnClickListener {
            play()
        }
    }
}